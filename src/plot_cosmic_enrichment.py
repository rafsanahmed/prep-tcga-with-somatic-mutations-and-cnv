import numpy as np
import os
import matplotlib.pyplot as plt
import math

def plot_hist_all_cosmic (filepath,dict1,title='',xlabel='',ylabel=''):

    fig,ax = plt.subplots()


    x = dict1.values()
    genes = dict1.keys()
    min_x = int(math.floor(min(x)))
    max_x = int(math.ceil(max(x)))

    r = np.arange(min_x, max_x+step, step)

    cg = []
    ncg = []
    # acg = []

    colors = ['C1','C0']
    lables = [ 'cosmic', 'non_cosmic']
    for i in range(len(x)):
        if genes[i] in cosmic_genes:
            cg.append(x[i])
        else:
            ncg.append(x[i])

    n, bins, patches = ax.hist([cg,ncg], bins=r, alpha=0.5, color=colors)
    # plt.hist(cg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.5, color='C1')
    # plt.hist(ncg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.5, color='C0')


    cosmic_dist = n[0]
    for cd,b in zip(cosmic_dist,bins):
        ax.annotate(str(int(cd)),xy=(b,0), fontsize=8,xycoords=('data', 'axes fraction'), xytext=(0,10),textcoords='offset points',va='top', ha='left')


    ax.legend()
    ax.grid()
    ax.set_title(title + ' COSMIC: {}'.format(len(cg)))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(np.arange(min_x, max_x+step*2, step*2))
    # ax.set_ylim(0,250)

    plt.show()
    # plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    # plt.close()

if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC',
                       'pancancer']

    step = 0.25
    inpath = '../data/MLA_intact_filtered/'
    cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'
    # '../data/ncg_genes/NCG6_cosmic_brca_genes.txt
    with open(cosmic_file, 'r') as f:
        cosmic_genes = set([line.split()[0] for line in f.readlines()[1:]])


    for i in range(len(cancer_subtypes)):
        c = cancer_subtypes[i]
        if c == 'COADREAD':
            outpath = '../out/figures/intact_filtered/' + c + '/'

            if not os.path.exists(outpath):
                os.makedirs(outpath)

            infile = inpath + c + '_MLA_standardized.txt'

            with open(infile,'r') as f:
                MLA_dict = {line.split()[0]:float(line.split()[1]) for line in f.readlines()}

            # plt.boxplot(MLA_dict.values(), positions=i)

            plot_hist_all_cosmic(outpath + c+'_mla_cosmic_dist.pdf',MLA_dict,title=c+'_MLA_distribution', xlabel='MLA',
                                 ylabel='Counts')