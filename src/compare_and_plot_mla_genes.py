import numpy as np
import os
import matplotlib.pyplot as plt


if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'COAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'READ', 'UCEC',
                       'pancancer']


    inpath = '../data/MLA_intact_filtered/'
    cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'
    with open(cosmic_file, 'r') as f:
        cosmic_genes = set([line.split()[0] for line in f.readlines()[1:]])

    genes = ['FGFR3','NFE2L2', 'ARID1A', 'STAG2','RB1', 'TP53', 'CDKN1A']
    for c in cancer_subtypes:
        if c == 'BLCA':

            infile = inpath + c + '_MLA_standardized.txt'

            with open(infile,'r') as f:
                MLA_dict = {line.split()[0]:float(line.split()[1]) for line in f.readlines()}

            g2 = {}
            for g in genes:
                g2[g] = MLA_dict[g]

            plt.scatter(g2.values(),g2.keys())
            plt.show()