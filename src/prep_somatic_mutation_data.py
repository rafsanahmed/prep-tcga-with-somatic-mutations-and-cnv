import pandas as pd
import numpy as np
import glob
from tqdm import tqdm
import os




if __name__ == '__main__':

    outpath = '../data/tcga_somatic_mutations_unfiltered/'
    pancancer_outfile = outpath + 'pancancer_patient_vs_genes_sm.txt'
    patient_count_outfile = outpath + 'patient_count.txt'

    _pf = open(patient_count_outfile, 'w')
    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']

    remove_variants = ['Silent', '3\'UTR', '5\'UTR', 'Intron', 'RNA', '3\'Flank','5\'Flank']
    remove_variants = [s.upper() for s in remove_variants]
    d_all = {}
    d_patients = {}
    for c in tqdm(cancer_subtypes):
        if 1:#c == 'BRCA':
            print c
            type_mutation_file = outpath + c + '_patient_vs_genes_sm.txt'
            inpath = '../data/tcga/' + c + '_oncotated/'


            file_lists = glob.glob(inpath + 'TCGA*.txt')
            patient_count = 0
            with open(type_mutation_file, 'w') as mf:
                for file_ in tqdm(file_lists):
                    directory, file_core = os.path.split(file_)
                    patient = file_core[:15]

                    mutations = set()
                    with open(file_, 'r')as f:
                        for line in f.readlines()[4:]:
                            line = line.split('\t')
                            mut = line[0].upper()
                            variant = line[8].upper()

                            if variant not in remove_variants and mut != 'UNKNOWN':
                                mutations.update([mut])

                    if len(mutations) != 0:
                        mf.write(patient)

                        for g in sorted(mutations):
                            mf.write('\t' + g.upper())
                        mf.write('\n')

                        patient_count +=1
                        if patient not in d_all:
                            d_all[patient] = mutations
                        else:
                            print "ERRRR!!!!!!!!"
                            print patient, len(mutations)
                            print len(d_all[patient])
                            quit()
            _pf.write(c + '\t' + str(patient_count) + '\n')



    with open(pancancer_outfile, 'w') as f:
        for k in sorted(d_all):
            f.write(k)
            for g in sorted(d_all[k]):
                f.write('\t' + g)
            f.write('\n')

    _pf.write('pancancer\t' + str(len(d_all)) + '\n')
    _pf.close()
