import numpy as np
from tqdm import tqdm, trange
import os

'''
Prepare binary matrices from raw somatic mutation data
'''

cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC', 'pancancer']
# cancer_subtypes = ['pancancer']

inpath = '../data/tcga_alterations_unfiltered/'
outpath = '../data/binary_matrices_alterations_unfiltered/'

if not os.path.exists(outpath):
    os.makedirs(outpath)

d_count = {}
for c in cancer_subtypes[:]:
    # c = cancer_subtypes[0]
    outfile = outpath + c + '_TML_binary_sm_cnv.txt'

    if 1:#c=='BRCA':
        infile = inpath + c+ '_patient_vs_genes_sm_cnv.txt'

        patient_vs_genes = {}
        patient_vs_genes_binary = {}
        patient_set = set()
        total_genes = set()

        with open(infile, 'r') as f:

            for line in f.readlines():
                line = line.split()
                patient_vs_genes[line[0]] = line[1:]
                total_genes.update(line[1:])
                patient_set.update([line[0]])

        genes = sorted(total_genes)
        with open(outfile, 'w') as f:
            f.write('patients')
            for g in genes:
                f.write('\t' + g)
            f.write('\ty\n')

            d_count[c] = len(patient_set)
            for p in tqdm(sorted(patient_set)):
                f.write(p)
                y = 0

                for g in sorted(total_genes):
                    if g in patient_vs_genes[p]:
                        f.write('\t1')
                        y+=1
                    else:
                        f.write('\t0')
                f.write('\t' + str(y) + '\n')



for k, v in sorted(d_count.iteritems()):
    print k, v