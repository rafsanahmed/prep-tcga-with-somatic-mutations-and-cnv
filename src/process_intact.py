import numpy as np
import os


if __name__ == '__main__':

    infile = '../data/human_intact_cleaned_18_JUN_2019.txt'
    intact_index_file = '../data/intact_index_file.txt'
    intact_edge_file = '../data/intact_edge_file.txt'

    threshold = 0.35

    edge_dict = {}
    with open(infile, 'r') as f:

        lines = f.readlines()
        print len(lines)
        for line in lines:
            line = line.split()

            g1 = line[0]
            g2 = line[1]
            if ':' in g1:
                g1 = g1.split(':')[1]
            if ':' in g2:
                g2 = g2.split(':')[1]

            edge_dict[g1 + ' ' + g2] = float(line[2])

    edge_dict2 = {}
    g2 = set()
    for e in edge_dict:

        if edge_dict[e] > threshold:
            edge_dict2[e] = edge_dict[e]
            g2.update(e.split())

    print len(edge_dict), len(edge_dict2), len(g2)

    gene_index_dict = {}
    with open(intact_index_file, 'w') as f:
        count = 0
        for g in sorted(g2):
            count +=1
            f.write(str(count) + '\t' + g.upper() +'\n')
            gene_index_dict[g.upper()] = count

    with open(intact_edge_file, 'w') as f:

        for e in sorted(edge_dict2.items(), key=lambda x: x[1],reverse=True):
            g1,g2 = e[0].split(' ')
            conf = e[1]

            f.write(str(gene_index_dict[g1.upper()]) + '\t' + str(gene_index_dict[g2.upper()]) + '\t' + str(conf) + '\n')
