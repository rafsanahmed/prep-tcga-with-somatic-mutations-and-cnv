import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
import glob
from tqdm import trange


def get_same_genes_list(A, B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    print ('set', len(s))

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)


def plot_heatmap(mla, mut_freq, step=0.25):
    x, y, genes = get_same_genes_list(mla, mut_freq)
    # #     d = get_same_genes_dict(mla,mut_freq)
    # #     print (d)
    df = pd.DataFrame(list(zip(genes, x, y)), columns=['Genes', 'MLA', 'freq'])
    # df2 = df.to
    # print (df2)
    # #     df = pd.DataFrame({'Genes':genes, 'MLA':x,'freq':y})

    #     df.set_index('Genes', inplace=True)
    #     print (df.head())

    #     df2 = pd.DataFrame(list(zip(x,y)),columns=['MLA','freq'])
    #     print (df2.head())
    # #     minx = math.floor(min(x))
    # #     maxx = math.ceil(max(x))
    # #     print (minx, maxx)
    # #     r = np.arange(minx,maxx+step, step)
    # #     print (minx, maxx, r)
    #     ds = sns.load_dataset(genes,x,y)
    # sns.heatmap(df2)
    mesh = np.meshgrid(x,y)
    plt.pcolormesh(x,y,mesh)
    plt.show()


if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC', 'pancancer']

    c = 'pancancer'
    model = 'mexcowalk_MLA_0.1'
    n = 500
    step_mla = 0.25
    step_freq = 0.05
    module =False

    freq_file = '../data/mutation_freq_sm_unfiltered/' + c + '_freq.txt'
    mla_file = '../data/MLA_unfiltered_logit_corrected/' + c + '_MLA_standardized.txt'

    cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'
    # ncg genes file : 'NCG6_cosmic_brca_genes.txt'#

    with open(cosmic_file, 'r') as f:
        cosmic_genes = [line.split()[0].upper() for line in f.readlines()[1:]]

    if module == True:
        print '../out/connected_components_isolarge/' + c + '_' + model + '/' + 'cc_n' + str(n) + '_*.txt'
        module_file = \
        glob.glob('../out/connected_components_isolarge/' + c + '_' + model + '/' + 'cc_n' + str(n) + '_*.txt')[0]
        with open(module_file, 'r') as f:
            module_genes = [g.upper() for line in f.readlines() for g in line.split()]
        with open(mla_file, 'r') as f:
            mla = {line.split()[0]: float(line.split()[1]) for line in f.readlines() if
                   line.split()[0] in module_genes}
        with open(freq_file, 'r') as f:
            freq = {line.split()[0]: float(line.split()[1]) for line in f.readlines() if
                    line.split()[0] in module_genes}
    else:
        with open(mla_file, 'r') as f:
            mla = {line.split()[0]: float(line.split()[1]) for line in f.readlines()}
        with open(freq_file, 'r') as f:
            freq = {line.split()[0]: float(line.split()[1]) for line in f.readlines()}

    mla_min = math.floor(min(mla.values()))
    mla_max = math.ceil(max(mla.values()))
    freq_min = math.floor(min(freq.values()))
    freq_max = (max(freq.values())) + step_freq

    print mla_min, mla_max, freq_min, freq_max

    rx = np.arange(mla_min, mla_max+step_mla,step_mla)
    ry = np.arange(freq_min, freq_max+step_freq,step_freq)

    heat_matrix = np.zeros((len(ry),len(rx)))
    cosmic_matrix = heat_matrix.copy()
    print heat_matrix.shape


    x,y,genes = get_same_genes_list(mla,freq)

    count = 0
    for idx in range(len(genes)):
        mla_temp =x[idx]
        freq_temp =y[idx]
        g =genes[idx]
        for i in range(len(rx)):

            for j in range(len(ry)):

                # print g, mla_temp, freq_temp, rx[i],ry[j]
                if (mla_temp>rx[i] and mla_temp<=rx[i]+step_mla) and (freq_temp>ry[j] and freq_temp<=ry[j]+step_freq):
                    count+=1

                    heat_matrix[j][i] += 1

                    if g in cosmic_genes:
                        cosmic_matrix[j][i] +=1
    print cosmic_matrix
    print count
    # sns.heatmap(heat_matrix)
    fig,ax = plt.subplots()
    # axiter = axes.flat
    # ax = axiter[0]
    # ax.pcolormesh(heat_matrix)
    # fig.colorbar(ax=ax)
    # ax.set_xticklabels(rx)
    # ax.set_yticklabels(ry)


    mesh = ax.pcolormesh(heat_matrix)
    # fig.colorbar(ax=ax)
    ax.locator_params(axis='x', nbins=len(rx))
    # xticks = ax.get_xticks()
    # print xticks
    # ax.set_xticks(rx)
    # xticks = ax.get_xticks()
    # print xticks
    # ax.set_yticks(ry)

    ax.set_xticklabels([round(temp,3) for temp in rx])
    ax.set_yticklabels([round(temp,3) for temp in ry])
    # ax.set_ylim(0,0.15)

    for i in range(len(rx)):
        for j in range(len(ry)):
            text = ax.text(i, j, str(int(heat_matrix[j][i])) + '\n' + str(int(cosmic_matrix[j][i])),
                           ha="left", va="bottom", color="C3", fontsize=8)


    print rx
    print ry
    # print heat_matrix


    fig.colorbar(mesh,ax=ax)

    plt.show()