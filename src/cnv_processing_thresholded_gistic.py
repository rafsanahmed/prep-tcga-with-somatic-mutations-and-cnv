import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import math

if __name__ == '__main__':

    inpath = '../data/cnv_hg19_thresholded/'


    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD','GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV','UCEC']

    for c in cancer_subtypes:
        cancer_type = c

        infile = inpath + cancer_type + '_all_thresholded.by_genes.txt'
        outfile = '../data/cnv_hg19_processed_patient_id_fixed/' + cancer_type + '.txt'


        df = pd.read_csv(infile, sep='\t', header=0, index_col=0)

        print df.head()

        # idx = df.index
        # print idx
        d = {}
        for col in tqdm(df.columns[2:]):

            cc = col[:15]
            if cc not in d:
                d[cc] = set()
            else:
                print 'cc',cc

            for i,v in tqdm(df[col].items()):

                if v==-2 or v==2:
                    if '|' in i:
                        d[cc].update([i.split('|')[0].upper()])
                    else:
                        d[cc].update([i.upper()])


        with open(outfile, 'w') as f:

            for col in sorted(d):
                print col, len(d[col])


                if len(d[col])!=0:
                    f.write(col)
                    for g in sorted(d[col]):
                        f.write('\t' + g)
                    f.write('\n')
