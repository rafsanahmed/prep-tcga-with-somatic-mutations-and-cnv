import pandas as pd
import numpy as np
import glob
from tqdm import tqdm
import os

'''
MLA: Only Somatic mutations
ALA: SM + CNV

Therefore, the code can be changed to only include SM

'''


if __name__ == '__main__':

    inpath1 = '../data/tcga_somatic_mutations_unfiltered/'
    inpath2 = '../data/cnv_hg19_processed_patient_id_fixed/'
    outpath = '../data/tcga_alterations_unfiltered/'#processed_mutation_data_intact_filtered/'
    outfile_temp = outpath +'all_genes.txt'
    outfile_all = outpath + 'pancancer_patient_vs_genes_sm_cnv.txt'
    # intact_index_file = '../data/intact_index_file.txt'
    patient_info_file = outpath + 'patient_info.txt'

    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']

    pat_inf = open(patient_info_file, 'w')

    ## uncomment to filter by intact
    # with open(intact_index_file, 'r') as f:
    #     ppi_genes = set()
    #     for line in f.readlines():
    #         line = line.split()
    #         ppi_genes.update([line[1].upper()])
    # print len(ppi_genes)
    genes = set()
    d_all = {}
    for c in tqdm(cancer_subtypes):
        print c
        outfile_c = outpath + c + '_patient_vs_genes_sm_cnv.txt'

        patient_vs_genes = {}

        ## get somatic mutations
        infile_sm = inpath1 + c + '_patient_vs_genes_sm.txt'
        with open(infile_sm, 'r') as f:
            for line in f.readlines():
                line = line.split()
                p = line[0]
                g = set([gene for gene in line[1:]])# if gene in ppi_genes])

                if len(g)!=0:
                    if p not in patient_vs_genes:
                        patient_vs_genes[p] = set()
                    if p not in d_all:
                        d_all[p] = set()

                    patient_vs_genes[p].update(g)
                    d_all[p].update(g)

                    genes.update(g)

        ## uncomment to include CNV
        infile_cnv = inpath2 + c + '.txt'
        with open(infile_cnv, 'r') as f:
            for line in f.readlines():
                line = line.split()
                p = line[0]
                g = set([gene for gene in line[1:]])# if gene in ppi_genes])

                if len(g) != 0:
                    if p not in patient_vs_genes:
                        patient_vs_genes[p] = set()
                    if p not in d_all:
                        d_all[p] = set()

                    patient_vs_genes[p].update(g)
                    d_all[p].update(g)

                    genes.update(g)

        print 'pvg',len(patient_vs_genes)

        pat_inf.write(c+'\t' + str(len(patient_vs_genes)) + '\n')
        with open(outfile_c, 'w') as f:

            for p in sorted(patient_vs_genes):
                f.write(p)
                for g in sorted(patient_vs_genes[p]):
                    f.write('\t' + g.upper())
                f.write('\n')

    print len(genes)
    print len(d_all)
    # write all_genes
    with open(outfile_temp, 'w') as f:
        for g in sorted(genes):
            f.write(g + '\n')
    with open(outfile_all, 'w') as f:

        for p in sorted(d_all):
            f.write(p)
            for g in sorted(d_all[p]):
                f.write('\t' + g.upper())
            f.write('\n')
    pat_inf.write('pancnacer\t' + str(len(d_all)) + '\n')
    pat_inf.close()