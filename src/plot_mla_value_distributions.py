import numpy as np
import os
import matplotlib.pyplot as plt


if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC',
                       'pancancer']

    inpath = '../data/MLA_unfiltered_logit_corrected/'

    vals = []
    for i in range(len(cancer_subtypes)):
        c = cancer_subtypes[i]
        outpath = '../out/figures/sm_unfiltered_logit_corrected/' + c + '/'

        if not os.path.exists(outpath):
            os.makedirs(outpath)

        infile = inpath + c + '_MLA_standardized.txt'

        with open(infile,'r') as f:
            MLA_dict = {line.split()[0]:float(line.split()[1]) for line in f.readlines()}

        # plt.boxplot(MLA_dict.values(), positions=i)
        vals.append(MLA_dict.values())

    plt.boxplot(vals, labels=cancer_subtypes)
    plt.title('MLA boxplots for unfiltered mutation data',fontsize=15)
    # plt.yticks(np.arange(-8,9,1))
    plt.ylabel('MLA')
    # plt.xlabel('Cancer Types')
    # plt.grid()
    plt.show()