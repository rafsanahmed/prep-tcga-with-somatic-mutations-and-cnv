import numpy as np
import os
from tqdm import tqdm, trange

'''
Prep cohort specific patient indices, gene vs patients files and mutations frequecy
files from 
'''

def get_gvp(pvg,idx):
    #both dict inputs

    d = {}
    patients = {}

    for pat in pvg:

        p = idx[pat]
        patients[pat] = p

        for g in pvg[pat]:
            # print g
            if g in d:
                d[g].update([p])

            else:
                d[g] = set([p])

    return (d,patients)



if __name__ == '__main__':

    '''
    inpath: mutation data (sm+CNV)
    inpath_pat: patient indices
    outpath_idx: cohort specific patient indices
    outpath_gvp: cohort specific gene vs patient indices
    outpath_freq: cohort specific mutation frequency of genes
    
    
    '''
    inpath = '../data/tcga_alterations_unfiltered/'#processed_mutation_data_intact_filtered/'
    infile_pat = '../data/patient_indices_alterations_unfiltered.txt'
    outpath_idx = '../data/patient_indices_alterations_unfiltered/'
    outpath_gvp = '../data/gene_vs_patients_alterations_unfiltered/'
    outpath_freq = '../data/mutation_freq_alterations_unfiltered/'

    if not os.path.exists(outpath_idx):
        os.makedirs(outpath_idx)
    if not os.path.exists(outpath_gvp):
        os.makedirs(outpath_gvp)
    if not os.path.exists(outpath_freq):
        os.makedirs(outpath_freq)

    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC',
                       'pancancer']

    with open(infile_pat, 'r') as f:
        patient_indices = {line.split()[0]: int(line.split()[1]) for line in f.readlines()}

    for i in trange(len(cancer_subtypes)):

        gene_set = set()
        c = cancer_subtypes[i]
        if 1:#c == 'BRCA':
            infile = inpath + c +'_patient_vs_genes_sm_cnv.txt'
            outfile_idx = outpath_idx + c + '_patient_indices.txt'
            outfile_gvp = outpath_gvp + c + '.txt'
            outfile_freq = outpath_freq + c + '_freq.txt'

            patients_vs_genes = {}
            with open(infile, 'r') as f:
                for line in f.readlines():
                    line = line.split()
                    patients_vs_genes[line[0]] = line[1:]

            genes_vs_patients, patients = get_gvp(patients_vs_genes,patient_indices)
            # print 'gvp',genes_vs_patients

            with open(outfile_gvp, 'w') as gvpf, open(outfile_freq, 'w') as frf:

                for g in sorted(genes_vs_patients):
                    frf.write(g + '\t' + str(float(len(genes_vs_patients[g]))/float(len(patients))) + '\n')
                    gvpf.write(g)
                    for p in sorted(genes_vs_patients[g]):
                        gvpf.write('\t' + str(p))
                    gvpf.write('\n')


            with open(outfile_idx, 'w') as f:
                for p in sorted(patients):
                    f.write(p + '\t' + str(patients[p]) + '\n')


