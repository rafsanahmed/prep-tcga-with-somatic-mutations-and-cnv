##steps

1. cnv_processing_thresholded_gistic.py
2. prep-somatic_mutation_data.py
3. prep_sm_cnv_all.py
4. binary_matrices_tml_pvg.py
5. logistic_regression_on subtype.ipynb
   5.a. compare_and_plot_mla_genes.py
   5.b. plot_cosmic_enrichment.py
6. compute_gene+v_pat_mut_freq.py