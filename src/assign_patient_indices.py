import numpy as np
from tqdm import tqdm, trange

c = 'pancancer'

infile = '../data/tcga_alterations_unfiltered/' + c + '_patient_vs_genes_sm_cnv.txt'
outfile = '../data/patient_indices_alterations_unfiltered.txt'
d = {}

count = 1
with open(infile, 'r') as f, open(outfile, 'w') as of:

    for line in f.readlines():

        line = line.split()
        of.write(line[0] + '\t' + str(count) + '\n')
        count+=1